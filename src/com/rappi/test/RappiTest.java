package com.rappi.test;

import java.util.ArrayList;
import java.util.InputMismatchException;

/**
 * @author Luis Andres Cordoba
 *
 */
public class RappiTest {

	private Cube cube;
	private InputCapture input;
	
	public RappiTest() {
		cube=new Cube();
		input = new  InputCapture();
	}
	
	public static void main(String[] args) 
	{
		RappiTest rappiTest=new RappiTest();
		rappiTest.runTest();

	}
	
	/**
	 * Method that hold the logic of the exercise.
	 */
	public void runTest() 
	{
		int numberOfTestCases;
		//Getting the number of test Cases (T)
		numberOfTestCases = input.captureInt("");
		String temp[];
		String stringTemp;
		ArrayList<String> resultList = new ArrayList<>();
		//Iterating over the number of test cases
		for (int i = 1; i <= numberOfTestCases; i++) {
			//Getting the size of the 3d matrix (N) and the number of operations (M)
			stringTemp=input.captureLine("");
			temp = stringTemp.split(" ");
			try
			{
				int size = Integer.parseInt(temp[0]);
				int numberOfOperations = Integer.parseInt(temp[1]);
				if (size > 0) {
					if (cube.creationOfTheCube(size)) {
						for (int j = 1; j <= numberOfOperations; j++) {
							temp = input.captureLine("").split(" ");
							if (temp[0].toUpperCase().equals("UPDATE"))
								cube.update(Integer.parseInt(temp[1]), Integer.parseInt(temp[2]), Integer.parseInt(temp[3]),
										Integer.parseInt(temp[4]));
							if (temp[0].toUpperCase().equals("QUERY")) //if user write the word QUERY, the program will call the method to query as required and it will add the result to an array list
								resultList.add("" + cube.query(Integer.parseInt(temp[1]), Integer.parseInt(temp[2]),
										Integer.parseInt(temp[3]), Integer.parseInt(temp[4]), Integer.parseInt(temp[5]),
										Integer.parseInt(temp[6])));

						}
						
					}
				}
			}
			catch (InputMismatchException | NumberFormatException e) {
				System.err.println("You must enter an integer for the size of the 3d matrix AND the number of operations. The program will keep going to the next test case.");
			}
		}
		//The program prints the results of the command query if any command was called by the user.
		for (String result : resultList) {
			System.out.println(result);
		}
	}
}
