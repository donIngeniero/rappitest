package com.rappi.test;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class in charge of capturing data from the keyboard
 * @author Luis Andres Cordoba
 *
 */
public class InputCapture {
	
	private Scanner keyboard;
	private int maximunAttempts;
	
	public int getMaximunAttempts() {
		return maximunAttempts;
	}

	public void setMaximunAttempts(int maximunAttempts) {
		this.maximunAttempts = maximunAttempts;
	}

	public InputCapture() {
		keyboard = new Scanner(System.in);
		maximunAttempts=5;
	}
	 
	public int captureInt(String message)
	{
		int numberToBeCapture = -1;
		int attempts = 0;
		while ( attempts < maximunAttempts)
		{
			System.out.println(message);
			try {
				numberToBeCapture = Integer.parseInt(keyboard.nextLine());
				return numberToBeCapture;
			} catch (InputMismatchException | NumberFormatException e) {
				System.err.println("You must enter an integer.");
			}
			attempts++;
		}
		return numberToBeCapture;
	}
	
	/**
	 * Method that capture a line from keyboard
	 * @param message
	 * @return
	 */
	public String captureLine(String message)
	{
			if(!message.isEmpty())
				System.out.println(message);		
			
			return keyboard.nextLine();
	}
}
