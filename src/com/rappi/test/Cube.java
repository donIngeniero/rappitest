/**
 * 
 */
package com.rappi.test;

/**
 * @author Luis Andres Cordoba
 *
 */
public class Cube {
	private int[][][] data;
	private int sizeOfTheMatrix;
	
	/**
	 * Method to create the 3d matrix receiving as argument the size 
	 * of the matrix
	 * @param size
	 * @return
	 */
	public boolean creationOfTheCube(int size)
	{
		//If the size is NOT greater than 0
		if(size<0)
		{
			System.err.println("The size of the 3D matriz must be greater than 0.");
			return false;
		}
		//If the size is greater than 0
		data=new int [size][size][size];
		//initialize matrix with zeros as value for each field
		for(int x=0;x<size;x++)
		{
			for(int y=0;y<size;y++)
			{
				for(int z=0;z<size;z++)
				{
					data[x][y][z]=0;
				}	
			}
		}
		sizeOfTheMatrix=size;
		return true;
	}
	
	/**
	 * Method that calculates the sum of the value of blocks whose x coordinate is between 
	 * x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z 
	 * coordinate between z1 and z2 (inclusive). 
	 * @param x1
	 * @param y1
	 * @param z1
	 * @param x2
	 * @param y2
	 * @param z2
	 * @return
	 */
	public int query(int x1, int y1, int z1, int x2, int y2, int z2)
	{
		int sum=0;
		for(int x=x1;x<=x2;x++)
		{
			for(int y=y1;y<=y2;y++)
			{
				for(int z=z1;z<=z2;z++)
				{
					sum+=data[x-1][y-1][z-1];
				}	
			}
		}
		return sum;
	}
	
	/**
	 * Method that updates the value of block (x,y,z) to a given value.
	 * @param x
	 * @param y
	 * @param z
	 * @param value
	 * @return
	 */
	public boolean update(int x, int y, int z, int value)
	{
		if(((x & y & z)>0)&((x & y & z)<=sizeOfTheMatrix))
		{
			data[x-1][y-1][z-1]=value;
			return true;
		}
		else
		{
			System.err.println("The arguments must be : 1 <= x,y,z <= (Size of the 3d matrix");
			return false;
		}
	}
}
